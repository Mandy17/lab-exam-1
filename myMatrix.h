//
// Created by Mandy on 9/26/2017.
//

#ifndef LAB_EXAM1_MYMATRIX_H
#define LAB_EXAM1_MYMATRIX_H
#include <iostream>
#include <string>

class myMatrix {
public:
        int rows;
        int columns;
       // int* matrix;
        double* matrix;
        myMatrix();
        myMatrix(int rows, int columns);
        myMatrix (int rows, int columns, std:: string input);
        ~myMatrix();
//        double getValye(int M, int N);
         myMatrix operator + (const myMatrix&);
//        operator + (const myMatrix& max1, const myMatrix& max2);
         myMatrix operator - (const myMatrix&);
         myMatrix operator * (const myMatrix&);
         friend std::ostream& operator <<(std:: ostream&&, const myMatrix&);
};


#endif //LAB_EXAM1_MYMATRIX_H
