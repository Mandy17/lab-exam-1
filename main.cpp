#include <iostream>
#include <string>
#include "myMatrix.h"

int main() {

    int M = 4;
    int N = 5;
    myMatrix matrix1(M, N); //sets a zero matrix
    myMatrix matrix2(4, 5, "random");
    myMatrix matrix3(4, 5, "allOne");

    std::cout << "matrix1: " << matrix1 << std::endl;
    std::cout << "matrix2: " << matrix2 << std::endl;

    std::cout << "matrix1 + matrix2 = " << matrix1 + matrix2 << std::endl;

    matrix3 = matrix1 + matrix2;
    std::cout << "matrix3: " << matrix3 << std::endl;

    myMatrix matrix4(5, 4, "allOne");
    std::cout << "matrix3 * matrix4 = " << matrix3 * matrix4 << std::endl;
return 0;
}
